function getCountries(){
	let dropdown = document.getElementById('locality-dropdown');
	dropdown.length = 0;

	let defaultOption = document.createElement('option');
	defaultOption.text = 'Choose State/Province';
	dropdown.style = 'none';


	dropdown.add(defaultOption);
	dropdown.selectedIndex = 0;

	const url = 'https://api.aircheckr.com/v1.5/territory/countries';

	fetch(url, {
		method: "GET",
		headers: {'x-access-token': 'eyJhbGciOiJIUzI1NiJ9.N2M0NzVmZjAtMzU2YS0xMWViLTg2OGMtMmY4OTMwN2VlOGQ1.YLATCY2jX0oDw7vyTsWghIdzCJCa8oqs_gaUI7o3Ubg'}})  
	.then(  
		function(response) {  
			if (response.status !== 200) {  
				console.warn('Looks like there was a problem. Status Code: ' + 
					response.status);  
				return;  
			}

      // Examine the text in the response  
      response.json().then(function(data) {  
      	let option;

      	for (let i = 0; i < data.length; i++) {
      		option = document.createElement('option');
      		let splitName = data[i].name + " ";
      		option.text = splitName.split(",")[0];
      		option.value = data[i].id;
      		dropdown.add(option);
      	}    
      });  
  }  
  )  
	.catch(function(err) {  
		console.error('Fetch Error -', err);  
	});
}

function getCities(sel) {
	let dropdown = document.getElementById('city-dropdown');
	dropdown.length = 0;
	var countryId = sel.options[sel.selectedIndex].value;
	dropdown.style = 'none';

	let defaultOption = document.createElement('option');
	defaultOption.text = 'Choose City';

	dropdown.add(defaultOption);
	dropdown.selectedIndex = 0;

	const url = 'https://api.aircheckr.com/v1.5/territory/' + countryId + '/names';

	fetch(url, {
		method: "GET",
		headers: {'x-access-token': 'eyJhbGciOiJIUzI1NiJ9.N2M0NzVmZjAtMzU2YS0xMWViLTg2OGMtMmY4OTMwN2VlOGQ1.YLATCY2jX0oDw7vyTsWghIdzCJCa8oqs_gaUI7o3Ubg'}})  
	.then(  
		function(response) {  
			if (response.status !== 200) {  
				console.warn('Looks like there was a problem. Status Code: ' + 
					response.status);  
				return;  
			}

      // Examine the text in the response  
      response.json().then(function(data) {  
      	let option;

      	for (let i = 0; i < data.length; i++) {
      		option = document.createElement('option');
      		option.text = data[i].name;
      		option.value = data[i].name;
      		dropdown.add(option);
      	}

      	showElement("city-dropdown");
      }); 

  }  
  )  
	.catch(function(err) {  
		console.error('Fetch Error -', err);  
	});
}

function getWeather(){
	var selectedCountry = document.getElementById('locality-dropdown').value;
	var selectedCity = document.getElementById('city-dropdown');
	var sel = selectedCity.value;
	let url = 'https://api.aircheckr.com/v1.5/territory/' + selectedCountry + '/name/' + sel;

	fetch(url, {
		method: "GET",
		headers: {'x-access-token': 'eyJhbGciOiJIUzI1NiJ9.N2M0NzVmZjAtMzU2YS0xMWViLTg2OGMtMmY4OTMwN2VlOGQ1.YLATCY2jX0oDw7vyTsWghIdzCJCa8oqs_gaUI7o3Ubg'}})
	.then(
		function(response) {
			if (response.status !== 200) {
				console.log('Looks like there was a problem. Status Code: ' +
					response.status);
				return;
			}

      // Examine the text in the response
      response.json().then(function(data) {
      	console.log(data.data[0]);
      	populateValues(data.data[0]);
      });
  }
  )
	.catch(function(err) {
		console.log('Fetch Error :-S', err);
	});
}

function showElement(elementId) {
	var x = document.getElementById(elementId);
	x.style.display = "block";
}

function populateValues(data){
	var aqi4 = document.getElementById('aqi4');
	aqi4.innerText = data.aqi_4.name;
	aqi4.style.color = data.aqi_4.color_hex;


	var aqi101 = document.getElementById('aqi11');
	aqi11.innerText = data.aqi_11.name;
	aqi11.style.color = data.aqi_11.color_hex;

	var aqi101 = document.getElementById('aqi101');
	aqi101.innerText = data.aqi_101;

	var aqi101no2 = document.getElementById('aqi101no2');
	aqi101no2.innerText = data.aqi_101_values.no2;

	var aqi101o3 = document.getElementById('aqi101o3');
	aqi101o3.innerText = data.aqi_101_values.o3;

	var aqi101pm10 = document.getElementById('aqi101pm10');
	aqi101pm10.innerText = data.aqi_101_values.pm10;

	var aqi101pm25 = document.getElementById('aqi101pm25');
	aqi101pm25.innerText = data.aqi_101_values.pm25;

	var recommendedSensitive = document.getElementById('recommendedSensitive');
	recommendedSensitive.innerText = data.recommend_sensitive.general;

	var recommendedNonSensitive = document.getElementById('recommendedNonSensitive');
	recommendedNonSensitive.innerText = data.recommend_non_sensitive.general;

	var recommendedSensitive = document.getElementById('recommendedSensitiveSports');
	recommendedSensitive.innerText = data.recommend_sensitive.sports;

	var recommendedNonSensitive = document.getElementById('recommendedNonSensitiveSports');
	recommendedNonSensitive.innerText = data.recommend_non_sensitive.sports;

	var recommendedSensitive = document.getElementById('recommendedSensitiveVentilation');
	recommendedSensitive.innerText = data.recommend_sensitive.ventilation;

	var recommendedNonSensitive = document.getElementById('recommendedNonSensitiveVentilation');
	recommendedNonSensitive.innerText = data.recommend_non_sensitive.ventilation;


}